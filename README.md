# Docker compose
---
Criar um arquivo `docker-compose.yml` e roda: `docker-compose up` para roda e segundo plano colocar o `docker-compose up -d`

* Para parar o faça: `docker-compose down`
* instalando o mysql e Adminer.

---
# Criando 3 aplicações com o docker-compose, aula muito importante Edivan. :)
link desta aula: <https://web.dio.me/course/docker-compose/learning/a42c1a14-e5fc-4af7-b7b2-e7e00c4e35cc?back=/track/cloud-devops-experience-banco-carrefour&tab=undefined&moduleId=undefined>
# 
---
# Ultilizando exemplos no docker-compose github.
https://github.com/docker/awesome-compose<br>


---
Link do material.
https://academiapme-my.sharepoint.com/personal/kawan_dio_me/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkawan%5Fdio%5Fme%2FDocuments%2FSlides%20dos%20Cursos%2FDocker%2FM%C3%B3dulo%202%2FCurso%202&ga=1

